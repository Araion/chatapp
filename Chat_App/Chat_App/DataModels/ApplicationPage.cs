﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chat_App.DataModels
{
    public enum ApplicationPage
    {
        Login = 0,
        Chat = 1,
    }
}
