﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chat_App.Models
{
    public class AuthenticatedUser
    {
        public int Id { get; set; }
        public string access_token { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
    }
}
