﻿using System.Threading.Tasks;
using Chat_App.Models;

namespace Chat_App.Helpers
{
    public interface IAPIHelper
    {
        Task<AuthenticatedUser> Authenticate(string username, string password);
    }
}