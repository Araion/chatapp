﻿using Chat_App.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Chat_App.Helpers
{
    public class ApiOperations
    {

        private string baseUrl;

        public ApiOperations()
        {
            this.baseUrl = "http://localhost:59592/api";
        }

        public AuthenticatedUser AuthenticateUser(string username, string password)
        {
            string endpoint = this.baseUrl + "/users/login";
            string method = "POST";
            string json = JsonConvert.SerializeObject(new
            {
                username = username,
                password = password
            });

            WebClient wc = new WebClient();
            wc.Headers["Content-Type"] = "application/json";
            try
            {
                string response = wc.UploadString(endpoint, method, json);
                return JsonConvert.DeserializeObject<AuthenticatedUser>(response);
            }
            catch (Exception)
            {
                return null;
            }
        }

        public AuthenticatedUser GetUserDetails(AuthenticatedUser user)
        {
            string endpoint = this.baseUrl + "/users/" + user.Id;
            string access_token = user.access_token;

            WebClient wc = new WebClient();
            wc.Headers["Content-Type"] = "application/json";
            wc.Headers["Authorization"] = access_token;
            try
            {
                string response = wc.DownloadString(endpoint);
                user = JsonConvert.DeserializeObject<AuthenticatedUser>(response);
                user.access_token = access_token;
                return user;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public AuthenticatedUser RegisterUser(string username, string password, string firstname,
    string lastname, string middlename, int age)
        {
            string endpoint = this.baseUrl + "/users";
            string method = "POST";
            string json = JsonConvert.SerializeObject(new
            {
                username = username,
                password = password,
                age = age
            });

            WebClient wc = new WebClient();
            wc.Headers["Content-Type"] = "application/json";
            try
            {
                string response = wc.UploadString(endpoint, method, json);
                return JsonConvert.DeserializeObject<AuthenticatedUser>(response);
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}
