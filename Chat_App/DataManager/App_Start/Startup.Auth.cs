﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.Google;
using Microsoft.Owin.Security.OAuth;
using Owin;
using DataManager.Providers;
using DataManager.Models;

namespace DataManager
{
    public partial class Startup
    {
        public static OAuthAuthorizationServerOptions OAuthOptions { get; private set; }

        public static string PublicClientId { get; private set; }

        // Další informace o konfiguraci ověřování najdete na webu https://go.microsoft.com/fwlink/?LinkId=301864.
        public void ConfigureAuth(IAppBuilder app)
        {
            // Konfigurovat kontext databáze a správce uživatelů, aby se používala jedna instance pro každý požadavek
            app.CreatePerOwinContext(ApplicationDbContext.Create);
            app.CreatePerOwinContext<ApplicationUserManager>(ApplicationUserManager.Create);

            // Povolit aplikaci používat soubor cookie k uložení informací pro přihlášeného uživatele
            // a používat soubor cookie k dočasnému uložení informací o uživateli přihlášeném pomocí zprostředkovatele přihlášení třetí strany
            app.UseCookieAuthentication(new CookieAuthenticationOptions());
            app.UseExternalSignInCookie(DefaultAuthenticationTypes.ExternalCookie);

            // Nakonfiguruje aplikaci pro tok na základě OAuth.
            PublicClientId = "self";
            OAuthOptions = new OAuthAuthorizationServerOptions
            {
                TokenEndpointPath = new PathString("/Token"),
                Provider = new ApplicationOAuthProvider(PublicClientId),
                AuthorizeEndpointPath = new PathString("/api/Account/ExternalLogin"),
                AccessTokenExpireTimeSpan = TimeSpan.FromDays(14),
                // V provozním režimu nastavte AllowInsecureHttp = false.
                AllowInsecureHttp = true
            };

            // Povolit aplikaci použití tokenů nosiče k ověřování uživatelů
            app.UseOAuthBearerTokens(OAuthOptions);

            // Zrušením komentáře u tohoto řádku povolíte protokolování s využitím zprostředkovatelů přihlášení třetích stran
            //app.UseMicrosoftAccountAuthentication(
            //    clientId: "",
            //    clientSecret: "");

            //app.UseTwitterAuthentication(
            //    consumerKey: "",
            //    consumerSecret: "");

            //app.UseFacebookAuthentication(
            //    appId: "",
            //    appSecret: "");

            //app.UseGoogleAuthentication(new GoogleOAuth2AuthenticationOptions()
            //{
            //    ClientId = "",
            //    ClientSecret = ""
            //});
        }
    }
}
